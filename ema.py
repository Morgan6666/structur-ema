
import numpy
from Bio.PDB.PDBParser import PDBParser
import pymol
import os
class StructureViewer:
    def __init__(self, filePath: str, filePath2: str = ''):
        self.parser = PDBParser()
        self.filePath2 = filePath2;
        self.filePath = filePath;
        self.structure = self.parser.get_structure(filePath, filePath)

    def calculateResidies(self, firstAcid: str, secondAcid: str, firstNumber: int, secondNumber: int) -> int:
        self.residues = [r for r in self.structure.get_residues()
                          if r.get_resname() == str(firstAcid) and r.get_id()[1] == int(firstNumber) 
                          or r.get_resname() == str(secondAcid) and r.get_id()[1] == int(secondNumber)]
        # Получаем координаты карбоксильной группы первой аминокислоты
        firstAcid = self.residues[0]["CA"].get_coord();
        # Получаем координаты карбоксильной группы второй аминокислоты
        secondAcid = self.residues[1]["CA"].get_coord();
        print(self.residues[0])
        print(self.residues[1])
        print('{} - {} = {}'.format(firstAcid,secondAcid, numpy.linalg.norm(firstAcid - secondAcid)));
        return numpy.linalg.norm(firstAcid - secondAcid)
    
if __name__ == '__main__':
    # Счимаем нормальное растояние между TYR74 и THR203
    struct = StructureViewer("fullPath/1ema.pdb", "fullPath/1cfp.pdb");
    ###struct.calculateResidies("TYR", "THR", 74, 203)
    ##Считаем мутантное расстояние между TYR74 и TYR203
    ###struct = StructureViewer("fullPath/1ema_A_THR203TYR.pdb");
    ###struct.calculateResidies("TYR", "TYR", 74, 203)

######## Скрипт для посчёта растояния #############################
  ############ Запуск ########################
     #### python ema.py ##########
        ### Настройки: поставьте заивисимости 
         #### Поставьте  pymol 
          #### укажите пути к файлам pdb
           #### Для запуска calculateResidies используейт позиции ваших аминокислот(пока доступно 2 аминокислоты, но можно расширить)


#############Растояние между TYR74 И THR203 ##########
# TYR 74 THR 203
# [<Residue TYR het=  resseq=74 icode= >, <Residue THR het=  resseq=203 icode= >]
# [35.861 16.659 44.69 ] - [35.879 27.067 37.499] = 12.650581359863281

###############Растояние между TYR74 и TYR203 ##########
# <Residue TYR het=  resseq=74 icode= >
# <Residue TYR het=  resseq=203 icode= >
# [35.861 16.659 44.69 ] - [35.879 27.067 37.499] = 12.650581359863281

####Задача 4
########Матрица цветов#############
###Красный-высокоактивная область
### Синий-нейтральный
### Белый-различие цветов
#### Картинка gydro.png

#####Задание 5
#### Фотка gydro.png в каталоге png

#### Задание 6
##### Фотка align_cfp_ema.png в каталоге png

#######Данные по мутациям доступны в mutation вместе с метированными pdb
##### Мутации произсодились инструментом haddok-tools/pdb_mutations
###### Список мутаций доступен в mutation.list
### Чтобы запустить скрипт нужно сначала собрать haddock-tools коммандой  cmake, затем запустить комманду
   ###### python ./pdb_mutate ../mutation.list
   ### https://github.com/haddocking/haddock-tools/tree/master
